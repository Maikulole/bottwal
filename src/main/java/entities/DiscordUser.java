package entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity @IdClass(DiscordUser.class)
public class DiscordUser implements Serializable {
    @Id
    private Long discordUserId;

    public DiscordUser(){}

    public DiscordUser(Long discordUserId){
        this.discordUserId = discordUserId;
    }

    public Long getDiscordUserId() {
        return discordUserId;
    }

    public void setDiscordUserId(Long discordUserId) {
        this.discordUserId = discordUserId;
    }

}
