package entities;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "LastFMUser.getLastFmUserByDiscordUser",
                query = "select l from LastFMUser l where l.discordUser.discordUserId = :discordUserId")
})
public class LastFMUser {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long userId;
    private String userName;
    private int userAge;
    private String userImage;
    @OneToOne
    private DiscordUser discordUser;
    public LastFMUser(){}

    public LastFMUser(String userName, int userAge, String userImage, DiscordUser discordUser){
        this.userName = userName;
        this.userAge = userAge;
        this.userImage = userImage;
        this.discordUser = discordUser;
    }
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public DiscordUser getDiscordUser() {
        return discordUser;
    }

    public void setDiscordUser(DiscordUser discordUser) {
        this.discordUser = discordUser;
    }
}
