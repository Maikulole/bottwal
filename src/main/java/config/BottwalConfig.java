package config;

import org.aeonbits.owner.ConfigFactory;

public final class BottwalConfig {

    private static BottwalConfig instance = null;

    private static IBottwalConfig config = ConfigFactory.create(IBottwalConfig.class);

    private BottwalConfig() {
    }

    public static IBottwalConfig get() {
        if (instance == null) {
            instance = new BottwalConfig();
        }
        return config;
    }
}
