package config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({"file:Bottwal/Config.properties"})
public interface IBottwalConfig extends Config {

    @Key("login.discordToken")
    String discordToken();

    @Key("login.lastfm")
    String lastFm();

    @Key("bot.prefix")
    String botPrefix();

    @Key("bot.ownerid")
    String ownerid();

    @Key("location.micha")
    String micha();

    @Key("location.lebron")
    String lebron();

    @Key("location.soeder")
    String soeder();

    @Key("location.moneyboy")
    String moneyboy();

    @Key("location.doto")
    String doto();
}
