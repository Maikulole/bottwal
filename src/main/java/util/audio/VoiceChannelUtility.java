package util.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import commands.voice.TTSCommand;

import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains a shitload of voice helper functions
 */
public class VoiceChannelUtility implements AudioLoadResultHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(VoiceChannelUtility.class);

    private GuildMusicManager guildMusicManager;
    private MessageChannel channel;

    public VoiceChannelUtility(GuildMusicManager guildMusicManager, MessageChannel channel) {
        this.guildMusicManager = guildMusicManager;
        this.channel = channel;
    }


    public void connectToVoiceChannel(VoiceChannel channel, AudioManager manager) {
        manager.openAudioConnection(channel);
    }

    public void loadAndPlay(String pathToFile, GuildMusicManager guildMusicManager, AudioPlayerManager manager) {
        manager.loadItemOrdered(guildMusicManager, pathToFile, this);
    }

    @Override
    public void trackLoaded(AudioTrack track) {
        play(guildMusicManager, track);
    }

    @Override
    public void playlistLoaded(AudioPlaylist playlist) {
        AudioTrack firstTrack = playlist.getSelectedTrack();

        if (firstTrack == null) {
            firstTrack = playlist.getTracks().get(0);
        }

        play(guildMusicManager, firstTrack);
    }

    @Override
    public void noMatches() {

    }

    @Override
    public void loadFailed(FriendlyException exception) {
        //TODO: IN Channel
        LOGGER.info("Track not found");
    }
    private void play(GuildMusicManager musicManager, AudioTrack track) {
        musicManager.getScheduler().queue(track);
    }

}

