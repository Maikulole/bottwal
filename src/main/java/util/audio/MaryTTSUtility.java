package util.audio;

import marytts.LocalMaryInterface;
import marytts.exceptions.MaryConfigurationException;
import marytts.exceptions.SynthesisException;
import marytts.util.data.audio.MaryAudioUtils;
import marytts.util.dom.DomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import util.files.xml.XMLBuilder;

import javax.sound.sampled.AudioInputStream;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Locale;

/**
 * Class for using MaryTTS to create funny stuff ok
 */
public class MaryTTSUtility {
    private static final Logger LOGGER = LoggerFactory.getLogger(MaryTTSUtility.class);
    private final String fileExtension = ".wav";
    public LocalMaryInterface marytts;

    /**
     * Creates a new marytts interface and sets options that we can use emotions.
     */
    public MaryTTSUtility() {
        try {
            this.marytts = new LocalMaryInterface();
        } catch (MaryConfigurationException e) {
            LOGGER.error("Error creating marytts interface: " + e.toString());
        }
        marytts.setLocale(Locale.GERMAN);
        marytts.setVoice("dfki-pavoque-neutral-hsmm");
        marytts.setInputType("EMOTIONML");
    }

    /**
     * Main entry point for mary tts
     * @param messageArray the tts message split by " "
     * @param messageid ID of the tts command message
     * @return path to the created wav file
     */
    public String maryTTSMain(String[] messageArray, String messageid) {
        //Keep the file name unique using the message id
        String pathToFile = "tts" + messageid + fileExtension;
        String emotionxml = XMLBuilder.buildXML(messageArray);

        Document xmlDocument = null;
        try {
            xmlDocument = DomUtils.parseDocument(emotionxml, false);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        AudioInputStream audio = generateAudioFromXML(xmlDocument);
        writeWAV(pathToFile, audio);

    return pathToFile;
    }

    /**
     * Generates a Audio Stream based on a given EmotionML file
     * @param xmlDocument the emotionml file
     * @return audiostream that marytts generated
     */
    private AudioInputStream generateAudioFromXML(Document xmlDocument) {
        AudioInputStream audio = null;
        try {
            audio = marytts.generateAudio(xmlDocument);
        } catch (SynthesisException e) {
            e.printStackTrace();
        }
        return audio;
    }

    /**
     * Writes a wav file from a given AudioInputStream to the disk
     * @param pathToFile the path to the file
     * @param audio the given audio input stream
     */
    private void writeWAV(String pathToFile, AudioInputStream audio) {
        double[] samples = MaryAudioUtils.getSamplesAsDoubleArray(audio);

        try {
            MaryAudioUtils.writeWavFile(samples, pathToFile, audio.getFormat());
        } catch (IOException e) {
            System.exit(1);
        }
    }
}
