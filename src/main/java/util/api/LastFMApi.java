package util.api;

import config.BottwalConfig;
import org.json.JSONObject;
import util.files.JSON;

import java.io.IOException;

/**
 * Utility class to interact with the last fm api
 */
public class LastFMApi {
    private static String baseUrl = "http://ws.audioscrobbler.com/2.0/?method=";
    private static String apiKeyPre = "&api_key=";
    private static String format = "&format=json";

    public static JSONObject getUserInformation(String username) {
        String requestURL = baseUrl + "user.getinfo&user=" + username + apiKeyPre + BottwalConfig.get().lastFm() + format;
        JSONObject response = null;
        try {
            response = JSON.readJsonFromUrl(requestURL).getJSONObject("user");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static JSONObject getLastTracks(String username, Boolean onlyMostRecent) {
        String requestURL = baseUrl + "user.getrecenttracks&user=" + username + apiKeyPre + BottwalConfig.get().lastFm() + format;
        JSONObject response = null;
        try {
            if (onlyMostRecent)
                response = JSON.readJsonFromUrl(requestURL).getJSONObject("recenttracks").getJSONArray("track").getJSONObject(0);
            else
                response = JSON.readJsonFromUrl(requestURL).getJSONObject("recenttracks");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
