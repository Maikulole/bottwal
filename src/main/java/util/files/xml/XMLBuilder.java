package util.files.xml;

import java.util.ArrayList;
import java.util.Random;

/**
 * Util class to build EmotionML files
 */
public final class XMLBuilder {
    private static String EMOTIONMLHEADER = "<emotionml version=\"1.0\" xmlns=\"http://www.w3.org/2009/10/emotionml\" category-set=\"http://www.w3.org/TR/emotion-voc/xml#everyday-categories\"> ";
    private static ArrayList<String> EMOTIONS = new ArrayList<String>() {
        {
            //From: https://www.w3.org/TR/emotion-voc/xml#big6
            /*
            add(" <emotion><category name=\"anger\"/> ");
            add(" <emotion><category name=\"disgust\"/> ");
            add(" <emotion><category name=\"fear\"/> ");
            add(" <emotion><category name=\"happiness\"/> ");
            add(" <emotion><category name=\"sadness\"/> ");
            add(" <emotion><category name=\"surprise\"/> "); */
            //https://www.w3.org/TR/emotion-voc/xml#everyday-categories
            add("  <emotion><category name=\"affectionate\"/> ");
            add("  <emotion><category name=\"afraid\"/> ");
            add("  <emotion><category name=\"amused\"/> ");
            add("  <emotion><category name=\"angry\"/> ");
            add("  <emotion><category name=\"confident\"/> ");
            add("  <emotion><category name=\"content\"/> ");
            add("  <emotion><category name=\"disappointed\"/> ");
            add("  <emotion><category name=\"excited\"/> ");
            add("  <emotion><category name=\"happy\"/> ");
            add("  <emotion><category name=\"interested\"/> ");
            add("  <emotion><category name=\"loving\"/> ");
            add("  <emotion><category name=\"pleased\"/> ");
            add("  <emotion><category name=\"sad\"/> ");
            add("  <emotion><category name=\"satisfied\"/> ");
            add("  <emotion><category name=\"worried\"/> ");
        }
    };
    private static String CLOSINGEMOTION = " </emotion> ";
    private static String CLOSINGEMOTIONML = " </emotionml> ";
    private static int EMOTIONLENGTH = 15;
    private static Random random = new Random();

    /**
     * Builds a EmotionML file containing random emotion switches
     * @param messageArray all words in an array
     * @return EmotionML XML File
     */
    public static String buildXML(String[] messageArray) {
        String emotionxml;
        int words = messageArray.length % EMOTIONLENGTH;
        if (words == 0)
            words = messageArray.length;
        emotionxml = EMOTIONMLHEADER + EMOTIONS.get(random.nextInt(EMOTIONS.size()));
        //Go through the whole message
        for (int j = 0; j < messageArray.length; j++) {
            emotionxml += messageArray[j] + " ";
            //After few words switch emotion
            if (j % words == 0 && j != 0) {
                emotionxml += CLOSINGEMOTION + EMOTIONS.get(random.nextInt(EMOTIONS.size()));
            }
        }
        emotionxml += CLOSINGEMOTION + CLOSINGEMOTIONML;
        return emotionxml;
    }
}
