package util.files;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JSON {

    private static final String USER_AGENT = "Mozilla/5.0";

    /**
     * Gets a JSONObject from a subreddit to read posts
     * @param url url to subreddit
     * @return JSONObject which holds als the post information
     * @throws IOException
     */
    public static JSONObject readJsonFromUrl(String url) throws IOException {
        URL uri = new URL(url);

        HttpURLConnection con = (HttpURLConnection) uri.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONTokener tokener = new JSONTokener(response.toString());
        return new JSONObject(tokener);
    }

}

