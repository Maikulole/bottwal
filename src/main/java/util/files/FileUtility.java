package util.files;

import java.io.File;
import java.util.Objects;
import java.util.Random;

/**
 * Helper class for file stuff
 */
public class FileUtility {
    private static Random random = new Random();

    /**
     * Gets a random file from a given folder
     * @param pathToFolder the path to the folder
     * @return file
     */
    public static File getFileFromPath(String pathToFolder) {
        final File dir = new File(pathToFolder);
        File[] files = dir.listFiles();
        //TODO: What happens if the folder is empty or does not exist, implement error check
        return Objects.requireNonNull(files)[random.nextInt(files.length)];
    }

    /**
     * Creates a String array with all the folders in a given location
     * @param pathToDirectory given path
     * @return the array
     */
    public static String[] getFoldersFromPath(String pathToDirectory) {
        File file = new File(pathToDirectory);
        String[] directories = file.list((current, name) -> new File(current, name).isDirectory());
        return directories;
    }
}
