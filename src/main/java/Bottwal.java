import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import commands.text.ChooseCommand;
import commands.text.LastFMCommand;
import commands.text.RandomImageCommand;
import commands.voice.DotoCommand;
import commands.voice.PlayCommand;
import commands.voice.TTSCommand;
import config.BottwalConfig;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.audio.GuildMusicManager;
import commands.voice.SkipCommand;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.security.auth.login.LoginException;
import java.util.HashMap;
import java.util.Map;

public class Bottwal {

    //Log variable
    private static final Logger LOGGER = LoggerFactory.getLogger(Bottwal.class);
    public static EntityManagerFactory emf;
    public static EntityManager em;

    //Connects the bot to discord
    public static void main(String[] args) throws LoginException {
        LOGGER.info("Initializing database connection");
        emf = Persistence.createEntityManagerFactory("bottwal");
        em = emf.createEntityManager();

        LOGGER.info("Starting Bottwal..");

        // define an eventwaiter, dont forget to add this to the JDABuilder!
        EventWaiter waiter = new EventWaiter();

        Map<Long, GuildMusicManager> musicManagers = new HashMap<>();
        AudioPlayerManager playerManager = new DefaultAudioPlayerManager();
        AudioSourceManagers.registerLocalSource(playerManager);
        AudioSourceManagers.registerRemoteSources(playerManager);

        CommandClientBuilder client = new CommandClientBuilder();
        client.setPrefix(BottwalConfig.get().botPrefix());
        client.setOwnerId(BottwalConfig.get().ownerid());
        client.addCommands(
                // add commands here
                new PlayCommand(musicManagers, playerManager),
                new TTSCommand(musicManagers, playerManager),
                new RandomImageCommand(),
                new ChooseCommand(),
                new DotoCommand(musicManagers, playerManager),
                new LastFMCommand(em),
                new SkipCommand(musicManagers, playerManager));

        // start getting a bot account set up
        new JDABuilder(AccountType.BOT)
                // set the token
                .setToken(BottwalConfig.get().discordToken())
                // set the game for when the bot is loading
                .setStatus(OnlineStatus.ONLINE)
                // add the listeners
                .addEventListeners(waiter, client.build())
                // start it up!
                .build();
    }
}
