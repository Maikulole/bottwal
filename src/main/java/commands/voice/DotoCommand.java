package commands.voice;

import com.google.common.eventbus.AllowConcurrentEvents;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import config.BottwalConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.audio.GuildMusicManager;
import util.audio.VoiceChannelUtility;
import util.files.FileUtility;

import java.io.File;
import java.util.Map;
import java.util.Random;

/**
 * Plays a random doto sound file.
 */
public final class DotoCommand extends Command {
    private static final Logger LOGGER = LoggerFactory.getLogger(DotoCommand.class);
    private final String pathToDotoFolder;
    private Map<Long, GuildMusicManager> musicManagers;
    private AudioPlayerManager manager;

    private Random random;
    public DotoCommand(Map musicManagers, AudioPlayerManager playerManager) {
        this.name = "doto";
        this.help = "Play a random doto sound or specify a hero, get a list of available heroes with !listheroes";
        this.arguments = "The hero";
        this.guildOnly = false;
        this.random = new Random();
        this.pathToDotoFolder = BottwalConfig.get().doto();
        this.musicManagers = musicManagers;
        this.manager = playerManager;
    }

    @Override
    protected void execute(CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong()).queue();
        String pathToDotoSound =  pathToDotoFolder;
        File dotoSound;

        GuildMusicManager guildMusicManager = null;
        //Get GuildMusicManager, Create new if none exists
        if(!musicManagers.containsKey(event.getGuild().getIdLong())){
            guildMusicManager = new GuildMusicManager(new DefaultAudioPlayerManager());
            musicManagers.put(event.getGuild().getIdLong(), guildMusicManager);
            event.getGuild().getAudioManager().setSendingHandler(musicManagers.get(event.getGuild().getIdLong()).getSendHandler());
        }else{
            guildMusicManager = musicManagers.get(event.getGuild().getIdLong());
        }

        //Check if hero is specified
        if ( event.getArgs() == "" ){
            String[] dotoHeros = FileUtility.getFoldersFromPath(pathToDotoFolder);
            pathToDotoSound += "\\" + dotoHeros[random.nextInt(dotoHeros.length)];
            LOGGER.info(pathToDotoSound);
            dotoSound = FileUtility.getFileFromPath(pathToDotoSound);

        } else {
            pathToDotoSound += "\\" + event.getArgs();
            LOGGER.info(pathToDotoSound);
            dotoSound = FileUtility.getFileFromPath(pathToDotoSound);
        }
        VoiceChannelUtility voiceChannelUtility = new VoiceChannelUtility(guildMusicManager, event.getChannel());
        voiceChannelUtility.connectToVoiceChannel(event.getMember().getVoiceState().getChannel(), event.getGuild().getAudioManager());
        voiceChannelUtility.loadAndPlay(dotoSound.getAbsolutePath(), guildMusicManager, manager);
    }
}
