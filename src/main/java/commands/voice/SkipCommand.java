package commands.voice;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import util.audio.GuildMusicManager;

import java.util.Map;

public class SkipCommand extends Command {
    private Map<Long, GuildMusicManager> musicManagers;
    private AudioPlayerManager manager;

    public SkipCommand(Map<Long, GuildMusicManager> musicManagers, AudioPlayerManager playerManager) {
        this.name = "skip";
        this.help = "Skip the current song";
        this.arguments = "Item to play";
        this.guildOnly = false;
        this.musicManagers = musicManagers;
        this.manager = playerManager;
    }

    @Override
    protected void execute(CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong()).queue();
        GuildMusicManager guildMusicManager = null;
        //Get GuildMusicManager, Create new if none exists
        if (!musicManagers.containsKey(event.getGuild().getIdLong())) {
            guildMusicManager = new GuildMusicManager(new DefaultAudioPlayerManager());
            musicManagers.put(event.getGuild().getIdLong(), guildMusicManager);
            event.getGuild().getAudioManager().setSendingHandler(musicManagers.get(event.getGuild().getIdLong()).getSendHandler());
        } else {
            guildMusicManager = musicManagers.get(event.getGuild().getIdLong());
        }
        guildMusicManager.getScheduler().nextTrack();
        event.getMessage().addReaction("⏭").queue();
    }
}
