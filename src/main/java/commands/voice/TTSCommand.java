package commands.voice;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import config.BottwalConfig;
import util.audio.GuildMusicManager;
import util.audio.MaryTTSUtility;
import util.audio.VoiceChannelUtility;

import java.util.Map;

/**
 * Creates a text to speech message and plays it in the callers voice channel.
 */
public final class TTSCommand extends Command {
    private Map<Long, GuildMusicManager> musicManagers;
    private AudioPlayerManager manager;
    //TODO Check if this creates a new mary interface each command call because that would be slow af
    private MaryTTSUtility maryTTSUtility;

    /**
     * Constructs a new tts command with all the shit needed
     * @param musicManagers containing all guildmusicmanager
     * @param playerManager the playermanager
     */
    public TTSCommand(Map musicManagers, AudioPlayerManager playerManager) {
        this.name = "tts";
        this.help = "Creates and plays a text to speech file";
        this.arguments = "The text to read";
        this.guildOnly = false;
        this.maryTTSUtility = new MaryTTSUtility();
        this.musicManagers = musicManagers;
        this.manager = playerManager;
    }

    /**
     * Command, builds the tts in a new thread so the shit bot isnt blocked all the time.
     * @param event the command event
     */
    @Override
    protected void execute(CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong()).queue();
        //Get TTS Message and remove the command call
        String ttsMessage = event.getMessage().getContentStripped().replace(BottwalConfig.get().botPrefix() + name, "");
        new Thread(() -> createWAVFile(ttsMessage.split(" "), event.getMessage().getId(), event)).start();
    }

    /**
     * Creates a .wav file and queues it to be played.
     * @param message message
     * @param event
     */
    public void createWAVFile(String[] message, String messageId, CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong());
        String pathToFile = maryTTSUtility.maryTTSMain(message, messageId);
        GuildMusicManager guildMusicManager = null;
        //Get GuildMusicManager, Create new if none exists
        if ( !musicManagers.containsKey(event.getGuild().getIdLong()) ) {
            guildMusicManager = new GuildMusicManager(new DefaultAudioPlayerManager());
            musicManagers.put(event.getGuild().getIdLong(), guildMusicManager);
            event.getGuild().getAudioManager().setSendingHandler(musicManagers.get(event.getGuild().getIdLong()).getSendHandler());
        } else {
            guildMusicManager = musicManagers.get(event.getGuild().getIdLong());
        }
        //Connect and play
        VoiceChannelUtility voiceChannelUtility = new VoiceChannelUtility(guildMusicManager, event.getChannel());
        voiceChannelUtility.connectToVoiceChannel(event.getMember().getVoiceState().getChannel(), event.getGuild().getAudioManager());
        voiceChannelUtility.loadAndPlay(pathToFile, guildMusicManager, manager);
    }

}

