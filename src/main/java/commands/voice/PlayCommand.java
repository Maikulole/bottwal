package commands.voice;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import net.dv8tion.jda.api.entities.Emote;
import util.audio.GuildMusicManager;
import util.audio.VoiceChannelUtility;

import java.util.Map;

public class PlayCommand extends Command {
    private Map<Long, GuildMusicManager> musicManagers;
    private AudioPlayerManager manager;

    public PlayCommand(Map<Long, GuildMusicManager> musicManagers, AudioPlayerManager playerManager) {
        this.name = "play";
        this.help = "Play a youtube link or some other stuff";
        this.arguments = "Item to play";
        this.guildOnly = false;
        this.musicManagers = musicManagers;
        this.manager = playerManager;
    }

    @Override
    protected void execute(CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong()).queue();
        if (event.getArgs().split(" ").length == 0) {
         event.getChannel().sendMessage("No item to play provided").queue();
        } else {
            String toPlay = event.getArgs().split(" ")[0];
            GuildMusicManager guildMusicManager = null;
            //Get GuildMusicManager, Create new if none exists
            if (!musicManagers.containsKey(event.getGuild().getIdLong())) {
                guildMusicManager = new GuildMusicManager(new DefaultAudioPlayerManager());
                musicManagers.put(event.getGuild().getIdLong(), guildMusicManager);
                event.getGuild().getAudioManager().setSendingHandler(musicManagers.get(event.getGuild().getIdLong()).getSendHandler());
            } else {
                guildMusicManager = musicManagers.get(event.getGuild().getIdLong());
            }

            VoiceChannelUtility voiceChannelUtility = new VoiceChannelUtility(guildMusicManager, event.getChannel());
            voiceChannelUtility.connectToVoiceChannel(event.getMember().getVoiceState().getChannel(), event.getGuild().getAudioManager());
            voiceChannelUtility.loadAndPlay(toPlay, guildMusicManager, manager);
            event.getMessage().addReaction(":arrow_forward:").queue();
        }
    }
}
