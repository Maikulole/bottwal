package commands.text;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import entities.DiscordUser;
import entities.LastFMUser;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageChannel;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.api.LastFMApi;

import javax.persistence.EntityManager;
import java.awt.*;

/** Command for LastFm statistics.
 */
public class LastFMCommand extends Command {
    private static final Logger LOGGER = LoggerFactory.getLogger(RandomImageCommand.class);
    private EntityManager em;
    public LastFMCommand(EntityManager em){
        this.name = "lastfm";
        this.help = "LastFM command for doing stuff with the api";
        this.arguments = "The text to read";
        this.guildOnly = false;
        this.em = em;
    }

    @Override
    protected void execute(CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong()).queue();
        String command = event.getArgs().split(" ")[0];
        Long userId = event.getAuthor().getIdLong();
        switch (command) {
            case "set" : setLastFmUser(userId, event.getArgs().split(" ")[1]); break;
            case "now" : lastFmNow(userId, event.getChannel()); break;
            case "recent" : lastFmRecent(userId, event.getChannel()); break;
            case "albumchart" : albumChart(event.getArgs().split(" ")[1], event.getChannel()); break;
            case "songchart" : songChart(event.getArgs().split(" ")[1], event.getChannel()); break;
            default: event.getChannel().sendMessage("Command not found, try help oder so").queue(); break;
        }
    }

    private void albumChart(String interval, MessageChannel channel) {
    }

    private void songChart(String interval, MessageChannel channel) {
    }

    /**
     * Gets the last 10 tracks and sends them as embedded message
     * @param userId the discord user id
     * @param channel the channel of the command
     */
    private void lastFmRecent(Long userId, MessageChannel channel) {
        LastFMUser lastFmUserName = (LastFMUser)  em.createNamedQuery("LastFMUser.getLastFmUserByDiscordUser").setParameter("discordUserId", userId).getSingleResult();
        JSONArray lastTracks = LastFMApi.getLastTracks(lastFmUserName.getUserName(), false).getJSONArray("track");
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.red);
        eb.setTitle(lastFmUserName.getUserName() + " last 10 Tracks");
        eb.setThumbnail(lastFmUserName.getUserImage());
        //TODO: URL not working in embedded
        eb.setFooter("[https://www.last.fm/user/" + lastFmUserName.getUserName() + "](" + lastFmUserName.getUserName() + ")");
        for(int i = 0; i < 10; i++) {
            JSONObject track = lastTracks.getJSONObject(i);
            eb.addField(track.getJSONObject("artist").getString("#text"), track.getString("name"), false);
        }
        channel.sendMessage(eb.build()).queue();
    }

    /**
     * Sends what the user is currently playing to the channel.
     * @param userId the discord user id to get the last fm account
     * @param channel discord channel to send the message
     */
    private void lastFmNow(Long userId, MessageChannel channel) {
        //TODO: Error handling if no user is set
        LastFMUser lastFmUserName = (LastFMUser) em.createNamedQuery("LastFMUser.getLastFmUserByDiscordUser").setParameter("discordUserId", userId).getSingleResult();
        //TODO Error handling if no track is found
        JSONObject lastTrack = LastFMApi.getLastTracks(lastFmUserName.getUserName(), true);
        EmbedBuilder eb = new EmbedBuilder();
        eb.setTitle(lastFmUserName + " is currently listening to:", null);
        eb.setColor(Color.red);
        //TODO: URL not working in embedded
        eb.setFooter("[https://www.last.fm/user/" + lastFmUserName.getUserName() + "](" + lastFmUserName.getUserName() + ")");
        eb.addField(lastTrack.getJSONObject("artist").getString("#text"), lastTrack.getString("name") + " - " + lastTrack.getJSONObject("album").getString("#text"), false);
        eb.setThumbnail(lastTrack.getJSONArray("image").getJSONObject(2).getString("#text"));
        channel.sendMessage(eb.build()).queue();
    }

    /**
     * Sets the last fm user
     * @param userId
     */
    private void setLastFmUser(Long userId, String lastFmName) {
        //TODO: Error handling
        JSONObject lastFmUser = LastFMApi.getUserInformation(lastFmName);
        String image = lastFmUser.getJSONArray("image").getJSONObject(3).getString("#text");
        DiscordUser discordUser = new DiscordUser();
        discordUser.setDiscordUserId(userId);
        LastFMUser lfmUser = new LastFMUser(lastFmName, lastFmUser.getInt("age"), image, discordUser);
        //Persists both users
        em.getTransaction().begin();
        em.persist(discordUser);
        em.persist(lfmUser);
        em.getTransaction().commit();
    }
}


