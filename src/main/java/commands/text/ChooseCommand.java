package commands.text;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;

import java.util.Random;

/**
 * Choose between given arguments
 */
public class ChooseCommand extends Command {
    private final Random random;

    /**
     * Initialize the choose command.
     */
    public ChooseCommand() {
        this.name = "choose";
        this.help = "Choose between things";
        this.arguments = "The things, seperated by commas";
        this.guildOnly = false;
        this.random = new Random();
    }

    /**
     * Gets a random argument, arguments are seperated by commas.
     * @param commandEvent the command event
     */
    @Override
    protected void execute(CommandEvent commandEvent) {
        //Error handling in case the user was too dumb to specify arguments
        if(commandEvent.getArgs().length() > 0) {
            commandEvent.getMessage().addReaction("🎲").queue();
            String[] arguments = commandEvent.getArgs().split(",");
            commandEvent.getChannel().sendMessage(arguments[random.nextInt(arguments.length)]).queue();
        } else {
            commandEvent.getChannel().sendMessage("Not enough arguments for the choose command").queue();
        }
    }
}
