package commands.text;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import config.BottwalConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.files.FileUtility;

import java.io.File;

/**
 * Posts a random image based on the command. Currently has soeder, lebron, burr, scurr and micha implemented.
 */
public final class RandomImageCommand extends Command {
    private static final Logger LOGGER = LoggerFactory.getLogger(RandomImageCommand.class);

    public RandomImageCommand() {
        this.name = "micha";
        this.aliases = new String[]{"soeder", "lebron", "burr", "scurr"};
        this.help = "Sends a random image";
        this.arguments = "no needed";
        this.guildOnly = false;
    }

    @Override
    protected void execute(CommandEvent event) {
        event.getChannel().deleteMessageById(event.getMessage().getIdLong()).queue();
        //Find out what kind of command was called
        String imageType = event.getMessage().getContentStripped().split(" ")[0].replace(BottwalConfig.get().botPrefix(), "");
        String pathToFolder = null;

        switch (imageType){
            case "micha" : pathToFolder = BottwalConfig.get().micha(); break;
            case "lebron" : pathToFolder = BottwalConfig.get().lebron(); break;
            case "soeder" : pathToFolder = BottwalConfig.get().soeder(); break;
            case "scurr" :
            case "burr" :
                pathToFolder = BottwalConfig.get().moneyboy(); break;
                default: event.getChannel().sendMessage("Sowas hab ich nicht").queue();
        }

        File image = FileUtility.getFileFromPath(pathToFolder);
        event.getChannel().sendFile(image, image.getName()).queue();

    }
}
